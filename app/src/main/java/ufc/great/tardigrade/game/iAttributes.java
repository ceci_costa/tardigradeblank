package ufc.great.tardigrade.game;

/**
 * Created by willianrodrigues on 18/08/2016.
 */
public interface iAttributes {
    void AppendAttribute(String name);
    void setAttribute(int index, Object value);
    Object getAttribute(int index);
    void removeAttribute(int index);
}
