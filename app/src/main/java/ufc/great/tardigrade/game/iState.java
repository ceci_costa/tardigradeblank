package ufc.great.tardigrade.game;


import java.util.List;

import ufc.great.components.Code;
import ufc.great.tardigrade.utils.iCallback;

public interface iState{
	void setOnMakeListener(iCallback callback);
	void setOnChangeListener(iCallback callback);
	void setOnUpdateListener(iCallback callback);

	interface iTuple {
		void append(Object object);
		Code getOperation();
		List<Object> retrieve();
	}
}
