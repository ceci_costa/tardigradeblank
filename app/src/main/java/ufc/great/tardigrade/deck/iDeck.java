package ufc.great.tardigrade.deck;

import java.io.IOException;
import java.util.List;

import ufc.great.tardigrade.utils.iCallback;

public interface iDeck{
	void init();
	
	void loadDeck() throws IOException;
	void shuffleDeck();
	
	void putCard(iCard card);
	iCard getCard(String id);
	List<iCard> getAllCards();
	
	void useCard(iCard card);

	void setOnLoadListener(iCallback callback);
	void setOnUseCardListener(iCallback callback);
}
