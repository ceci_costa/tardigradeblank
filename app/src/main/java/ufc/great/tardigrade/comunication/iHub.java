package ufc.great.tardigrade.comunication;

import ufc.great.tardigrade.utils.iCallback;

public interface iHub {
	void start();
	void stop();

	boolean isConnected();

	void setOnChangeListener(iCallback callback);
	void setOnAwareListener(iCallback callback);
}