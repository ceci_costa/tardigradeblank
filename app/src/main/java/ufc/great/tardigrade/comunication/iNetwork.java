package ufc.great.tardigrade.comunication;

import ufc.great.tardigrade.utils.iCallback;

public interface iNetwork {
	void start();
	void stop();
	int getPort();
	
	void setOnReceiveListener(iCallback callback);
	boolean isWorking();
}
