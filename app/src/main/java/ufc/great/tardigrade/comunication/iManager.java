package ufc.great.tardigrade.comunication;

import ufc.great.tardigrade.utils.iCallback;

public interface iManager {
	void registerObserver(iChannel channel);
	void removeObserver(iChannel channel);
	void notifyObserver(iPack pack);

	void update(iPack pack);

	void setOnObserverListChangeListener(iCallback callback);
}