package ufc.great.tardigrade.comunication;

public interface iPack{
    void setValue(Object value);
    Object getValue();

    void setKey(String key);
    String getKey();
}
