package ufc.great.game;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import ufc.great.tardigradeteste.R;

public class MainActivity extends Activity {

    TextView data;
    Button buttonReadQR;
    Switch switchDiscovery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data = (TextView) findViewById(R.id.textViewValue);
        buttonReadQR = (Button) findViewById(R.id.buttonRead);
        switchDiscovery = (Switch) findViewById(R.id.switchDiscovery);

        /**
         * Sua programação começa aqui
         */

        // Programe aqui :)

        /**
         * Sua programação termina aqui
         */

        buttonReadQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ação do botão
            }
        });
        switchDiscovery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                // ação quando altera o switch
            }
        });
    }

    public void toasting(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void changeData(final String text){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                data.setText(text);
            }
        });
    }
}
