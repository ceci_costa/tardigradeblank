package ufc.great.components.dialog;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ufc.great.tardigrade.utils.StatusListener;
import ufc.great.tardigradeteste.R;

public class TimerDialog extends Dialog {
    private int time = 5000;
    private ProgressBar timer;
    private ObjectAnimator updateTimer;
    private ValueAnimator updateValue;

    private boolean showCounter = true;
    private boolean canClose = false;

    private RelativeLayout startTouch;
    private LinearLayout infoView;
    private LinearLayout valueView;

    private TextView textTitle;
    private TextView textTimerValue;
    private TextView textWaiting;
    private TextView textValuePre;
    private TextView textValue;
    private TextView textValuePos;
    private TextView textAdvice;
    private TextView textText;
    private TextView textTip;

    private int textResource;
    private int tipResource;
    private int imageResource;


    private ImageView imageTip;

    private StatusListener mCallback;

    private TimerDialog(Context context) {
        super(context);
    }

    public static TimerDialog create(Context context){
        return new TimerDialog(context);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.component_verification_dialog);

        if(!getContext().getResources().getBoolean(R.bool.landscape)){
            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        }else {
            getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.MATCH_PARENT);
        }

        setCancelable(false);

        textTitle = (TextView) findViewById(R.id.textViewTitle);
        textValuePre = (TextView) findViewById(R.id.textViewValuePre);
        textValue = (TextView) findViewById(R.id.textViewValue);
        textValuePos = (TextView) findViewById(R.id.textViewValuePos);
        textWaiting = (TextView) findViewById(R.id.textViewWaiting);
        textTimerValue = (TextView) findViewById(R.id.textViewTimer);
        textAdvice = (TextView) findViewById(R.id.textViewAdvice);
        textText = (TextView) findViewById(R.id.textViewText);
        textTip = (TextView) findViewById(R.id.textViewTip);

        imageTip = (ImageView) findViewById(R.id.imageViewImage);

        startTouch = (RelativeLayout) findViewById(R.id.StartTouch);
        infoView = (LinearLayout) findViewById(R.id.infoView);
        valueView = (LinearLayout) findViewById(R.id.valueView);

        timer = (ProgressBar) findViewById(R.id.progressBarTimer);

        if(time == -1) {
            time = 100;
        }

        if(time == -1) {
            timer.setIndeterminate(true);
        }else {
            timer.setMax(time);
            timer.setProgress(0);

            updateTimer = ObjectAnimator.ofInt(timer, "progress", 0, time);
            updateTimer.setDuration(time);
            updateTimer.setInterpolator(new LinearInterpolator());
            updateTimer.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {}
                @Override
                public void onAnimationEnd(Animator animator) {
                    stop();
                }
                @Override
                public void onAnimationCancel(Animator animator) {}
                @Override
                public void onAnimationRepeat(Animator animator) {}
            });

            updateValue = ValueAnimator.ofInt(time/1000, 0);
            updateValue.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    textTimerValue.setText(valueAnimator.getAnimatedValue().toString());
                }
            });
            updateValue.setDuration(time);
            updateValue.setInterpolator(new LinearInterpolator());
        }

        startTouch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(canClose){
                    mCallback.onEnd(null);
                    dismiss();
                }else {
                    timer.setBackgroundResource(R.drawable.bg_progress_bar);
                    startTouch.setClickable(false);
                    start();
                }
            }
        });

        textText.setText(textResource);
        textTip.setText(tipResource);
        imageTip.setImageResource(imageResource);
        textTimerValue.setText(time+"");
        toogleViews(false);
    }

    public void setProps(int textResource, int tipResource, int imageResource, int time){
        this.textResource = textResource;
        this.tipResource = tipResource;
        this.imageResource = imageResource;
        this.time = time;
    }

    public void start(){
        toogleViews(true);
        if(time != -1) {
            updateTimer.start();
            updateValue.start();
        }

        mCallback.onStart(null);
    }

    public void toogleViews(boolean enabled){
        if(enabled){
            infoView.setVisibility(View.GONE);
            valueView.setVisibility(View.VISIBLE);
            textAdvice.setVisibility(View.INVISIBLE);
            textWaiting.setVisibility(View.VISIBLE);
            textValue.setVisibility(View.GONE);
            textTitle.setText(R.string.title_doing);

            if(showCounter && time != -1){
                textTimerValue.setVisibility(View.VISIBLE);
            }
        }else{
            infoView.setVisibility(View.VISIBLE);
            valueView.setVisibility(View.GONE);
            textTitle.setText(R.string.title_init);
            textTimerValue.setVisibility(View.GONE);
        }
    }

    public void changing(){
        textWaiting.setVisibility(View.GONE);
        textValue.setVisibility(View.VISIBLE);
    }

    public void changeValue(String pre, String content, String pos, Object raw){
        changing();
        textValue.setTextSize(getContext().getResources().getDimension(R.dimen.dialog_title_text_size));

        textValuePre.setText(pre);
        textValue.setText(content);
        textValuePos.setText(pos);

        timer.setBackgroundResource(R.drawable.bg_progress_bar_end_on);
        startTouch.setClickable(true);
    }

    public void changeValue(final String[] list, final String pre, final int index, final String pos){
        changing();
        textValue.setTextSize(20f);
        textValuePre.setText(pre);
        textValuePos.setText(pos);

        ValueAnimator changing = ValueAnimator.ofInt(0, list.length - 1);
        changing.setDuration(1000);
        changing.setInterpolator(new LinearInterpolator());
        changing.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                textValue.setText(list[(int)valueAnimator.getAnimatedValue()]);
            }
        });
        changing.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {}
            @Override
            public void onAnimationEnd(Animator animator) {
                timer.setBackgroundResource(R.drawable.bg_progress_bar_end_on);
                textValue.setText(list[index]);
                startTouch.setClickable(true);
            }
            @Override
            public void onAnimationCancel(Animator animator) {}
            @Override
            public void onAnimationRepeat(Animator animator) {}
        });

        changing.start();
    }

    public void changeValue(final String pre, final int value, final String signal, final String pos){
        changing();
        textValuePre.setText(pre);
        textValuePos.setText(pos);

        ValueAnimator changing = ValueAnimator.ofInt(0, value);
        changing.setDuration(1000);
        changing.setInterpolator(new AccelerateInterpolator());
        changing.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
            textValue.setText(valueAnimator.getAnimatedValue().toString() + signal);
            }
        });
        changing.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {}
            @Override
            public void onAnimationEnd(Animator animator) {
                timer.setBackgroundResource(R.drawable.bg_progress_bar_end_on);
                startTouch.setClickable(true);
            }
            @Override
            public void onAnimationCancel(Animator animator) {}
            @Override
            public void onAnimationRepeat(Animator animator) {}
        });
        changing.start();
    }

    public void stop(){
        textTimerValue.setText("");
        timer.setBackgroundResource(R.drawable.bg_progress_bar_end_off);
        mCallback.onUpdate(null);
        canClose = true;
    }

    public void setStatusListener(StatusListener callback){
        mCallback = callback;
    }
}