package ufc.great.components.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import ufc.great.tardigradeteste.R;

/** HOW USE IT
 *
 *  PromptDialog mPromptDialog = new PromptDialog(c);
 *  mPromptDialog.setDescription("label or message for dialog box." + p.getData()).setImage(R.drawable.pencil);
 *  mPromptDialog.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
 *      @Override
 *      public void onClick(DialogInterface dialogInterface, int i) {
 *          String value = mPromptDialog.getDate();
 *          // Confirm action
 *      }
 *  });
 *  mPromptDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
 *      @Override
 *      public void onClick(DialogInterface dialogInterface, int i) {
 *          // Cancel action
 *      }
 *  });
 *  mCustomDialog.create().show();
 *
 */

public class PromptDialog extends AlertDialog.Builder {
    private String description = "";
    private String data = "";

    private TextView descriptionView;
    private EditText dataView;
    private ImageView imageView;

    public PromptDialog(Context context) {
        super(context);

        LayoutInflater li = LayoutInflater.from(context);
        View view = li.inflate(R.layout.component_prompt, null);

        descriptionView = (TextView) view.findViewWithTag("title");
        dataView = (EditText) view.findViewWithTag("data");
        imageView = (ImageView) view.findViewWithTag("image");

        this.setView(view);
    }


    public PromptDialog setDescription(String description){
        if(description == null){
            this.descriptionView.setVisibility(View.GONE);
        }else {
            this.descriptionView.setVisibility(View.VISIBLE);
            this.description = description;
            descriptionView.setText(description);
        }

        return this;
    }

    public PromptDialog setImage(int DrawableResource){
        if(DrawableResource != 0) {
            this.imageView.setVisibility(View.VISIBLE);
            this.imageView.setImageResource(DrawableResource);
        }else{
            this.imageView.setVisibility(View.GONE);
        }
        return this;
    }

    public String getData(){
        return this.dataView.getText().toString();
    }
}