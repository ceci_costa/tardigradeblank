package ufc.great.components.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ufc.great.tardigradeteste.R;

/** HOW USE IT
 *
 *  CustomDialog mCustomDialog = new CustomDialog(c);
 *  mCustomDialog.setDescription("label or message for dialog box").setImage(R.drawable.pencil);
 *  mCustomDialog.create().show();
 *
 */

public class CustomDialog extends AlertDialog.Builder {
    private String description = "";

    private TextView descriptionView;
    private ImageView imageView;

    public CustomDialog(Context context) {
        super(context);

        LayoutInflater li = LayoutInflater.from(context);
        View view = li.inflate(R.layout.component_dialog, null);

        descriptionView = (TextView) view.findViewWithTag("title");
        imageView = (ImageView) view.findViewWithTag("image");

        this.setView(view);
    }


    public CustomDialog setDescription(String description){
        if(description == null){
            this.descriptionView.setVisibility(View.GONE);
        }else {
            this.descriptionView.setVisibility(View.VISIBLE);
            this.description = description;
            descriptionView.setText(description);
        }

        return this;
    }

    public CustomDialog setImage(int DrawableResource){
        if(DrawableResource != 0) {
            this.imageView.setVisibility(View.VISIBLE);
            this.imageView.setImageResource(DrawableResource);
        }else{
            this.imageView.setVisibility(View.GONE);
        }
        return this;
    }
}