package ufc.great.components;

import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Vibrator;

public class HapticFeedback {
    private static HapticFeedback ourInstance = null;
    private Vibrator mVibrator;

    public static HapticFeedback getInstance(Context context) {
        if(ourInstance == null){
            if(context != null){
                ourInstance = new HapticFeedback(context);
            }
        }
        return ourInstance;
    }

    private HapticFeedback(Context context){
        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public void vibrate(int time){
        mVibrator.vibrate(time);
    }

    public static void beep(int tone){
        final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 200);
        tg.startTone(tone);
    }
}