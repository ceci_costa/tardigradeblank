package ufc.great.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import ufc.great.tardigrade.comunication.iChannel;
import ufc.great.tardigradeteste.R;

public class DeviceComponent extends LinearLayout {
    private String ID_Player;
    private TextView labelName;
    private TextView labelID;
    private Button buttonJoin;
    private Context context;

    public iChannel channel;

    public DeviceComponent(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public DeviceComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public DeviceComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    public void init(){
        View.inflate(context, R.layout.component_device, this);
        labelName = (TextView) findViewWithTag("name");
        labelID = (TextView) findViewWithTag("ip");
        buttonJoin = (Button) findViewWithTag("button");
    }

    public Button getJoinButton(){
        return buttonJoin;
    }
    public TextView getLabelName(){
        return labelName;
    }
    public void setIP(String ip){
        labelID.setText(ip);
    }

    public void setPlayerID(String ID){
        ID_Player = ID;
    }
    public String getPlayerID(){
        return ID_Player;
    }
}
