package ufc.great.components.aware;

import java.util.Random;

import ufc.great.tardigrade.utils.StatusListener;

public class Dice {
    private StatusListener mCallback;

    public Dice(StatusListener callback) {
        mCallback = callback;
    }

    public void start(int min, int max, int times) {
        mCallback.onStart(null);
        for (int i = 0; i < times; i++) {
            mCallback.onUpdate(get(max - min) + min);
        }
        mCallback.onEnd(null);
    }

    public static int getOne(int min, int max) {
        return get(max - min) + min;
    }

    protected static int get(int max) {
        return (new Random().nextInt(10000*max)) % (max + 1);
    }
}