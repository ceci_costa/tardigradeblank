package ufc.great.components.aware;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import ufc.great.tardigrade.utils.StatusListener;
import ufc.great.tardigradeteste.R;

public class Light {
	private boolean update = false;

	private Sensor sensor;
	private SensorManager sensorManager;
	private SensorListener sensorListener;
	private StatusListener mCallback;

	public static int [] labels = { R.string.illumination_low, R.string.illumination_normal, R.string.illumination_high };

	public static int getLabelIndex(float value){
		if(value <= 10){
			return 0;
		}else {
			if(value <= 100){
				return 1;
			}else{
				return 2;
			}
		}
	}

	public Light(Context context, StatusListener callback){
		mCallback = callback;

		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

		sensorListener = new SensorListener();

		if(sensor != null){
			sensorManager.registerListener(sensorListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		}
	}

	public void start(){
		update = true;
		mCallback.onStart(null);
	}

	public void stop(){
		update = false;
		sensorManager.unregisterListener(sensorListener);
		mCallback.onEnd(null);
	}
	
	private class SensorListener implements SensorEventListener {
		public void onAccuracyChanged(Sensor sensor, int arg1) { }
		public void onSensorChanged(SensorEvent event ) {
			if(update) {
				mCallback.onUpdate(event.values[0]);
			}
		}
	}
}

