package ufc.great.components.aware;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;

import ufc.great.tardigrade.utils.StatusListener;
import ufc.great.tardigradeteste.R;

public class Climate {
    private StatusListener mCallback;

    private Activity mContext;
    private GoogleApiClient mGoogleApiClient;

    public static int[] Conditions = {
            R.string.weather_condition_unknown,
            R.string.weather_condition_clear,
            R.string.weather_condition_cloudy,
            R.string.weather_condition_foggy,
            R.string.weather_condition_hazy,
            R.string.weather_condition_icy,
            R.string.weather_condition_rainy,
            R.string.weather_condition_snowy,
            R.string.weather_condition_stormy,
            R.string.weather_condition_windy };

    public Climate(Activity context, StatusListener callback) {
        mCallback = callback;
        mContext = context;

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Awareness.API)
                .build();
        mGoogleApiClient.connect();
    }

    public void start() {
        mCallback.onStart(null);
        getWeather();
    }

    public void stop(){
        mCallback.onEnd(null);
        mGoogleApiClient.disconnect();
    }

    public void getWeather(){
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 00011);
        }

        Awareness.SnapshotApi.getWeather(mGoogleApiClient)
            .setResultCallback(new ResultCallback<WeatherResult>() {
                @Override
                public void onResult(@NonNull WeatherResult weatherResult) {
                    if (!weatherResult.getStatus().isSuccess()) {
                        return;
                    }
                    Weather weather = weatherResult.getWeather();
                    mCallback.onUpdate(weather);
                    stop();
                }
            });
    }

    public static class GPSTest{
        Activity mContext;
        private StatusListener mReadyCallback;

        public GPSTest(Activity context, StatusListener callback){
            mReadyCallback = callback;
            mContext = context;

            LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                mReadyCallback.onStart(true);
            } else {
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 00011);
                    ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 00011);
                }
                manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new TesteLocation());
            }
        }

        private class TesteLocation implements LocationListener {
            @Override
            public void onLocationChanged(Location location) { }
            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) { }
            @Override
            public void onProviderEnabled(String s) {   mReadyCallback.onUpdate(true);  }
            @Override
            public void onProviderDisabled(String s) {  mReadyCallback.onUpdate(false); }
        }
    }
}
