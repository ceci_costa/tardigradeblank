package ufc.great.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class SizerLayout extends LinearLayout {

    public SizerLayout(Context context) {
        super(context);
    }

    public SizerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SizerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = (width/16) * 9;
        setMeasuredDimension(width, height);
        
        super.onMeasure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
    }
}