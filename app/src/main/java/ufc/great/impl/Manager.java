package ufc.great.impl;

import java.util.ArrayList;
import java.util.List;

import ufc.great.tardigrade.comunication.iChannel;
import ufc.great.tardigrade.comunication.iManager;
import ufc.great.tardigrade.comunication.iPack;
import ufc.great.tardigrade.utils.Flag;
import ufc.great.tardigrade.utils.NullCallback;
import ufc.great.tardigrade.utils.iCallback;

public class Manager implements iManager {
    private static Manager ourInstance = new Manager();
    private List<iChannel> opponents;
    private int maxOpponents = 1;
    private iCallback onChange;

    private Manager(){
        onChange = new NullCallback();
        opponents = new ArrayList<iChannel>();
    }

    public static Manager getInstance() {
        return ourInstance;
    }

    @Override
    public void registerObserver(iChannel channel) {
        if(opponents.size() < maxOpponents){
            opponents.add(channel);
            onChange.callingBack(Pack.create(Flag.START_GAME, channel));
        }
    }

    @Override
    public void removeObserver(iChannel channel) {
        for (iChannel opponent: opponents) {
            String id_opponent = opponent.getName().split(":")[0];
            String id_channel = channel.getName().split(":")[0];

            if(id_opponent.equals(id_channel)) {
                onChange.callingBack(Pack.create(Flag.END_GAME, channel));
                opponents.remove(opponent);
            }
        }
    }

    @Override
    public void setOnObserverListChangeListener(iCallback callback) {
        onChange = callback;
    }

    @Override
    public void notifyObserver(iPack pack) {
        for (iChannel opponent: opponents) {
            opponent.send(pack);
        }
    }

    public List<String> getObserverNames(){
        List<String> names = new ArrayList<String>();

        for (iChannel opponent: opponents) {
            names.add(opponent.getName().split(":")[1]);
        }
        return names;
    }

    public List<String> getObserverIds(){
        List<String> ids = new ArrayList<String>();

        for (iChannel opponent: opponents) {
            ids.add(opponent.getName().split(":")[0]);
        }
        return ids;
    }

    @Override
    public void update(iPack pack) {
        // Recebeu pacote
    }
}