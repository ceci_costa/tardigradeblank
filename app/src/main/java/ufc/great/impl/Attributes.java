package ufc.great.impl;

import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

import ufc.great.tardigrade.game.iAttributes;

public class Attributes implements iAttributes {
    protected List<Pair<String, Object>> attributes;

    public static List<String> ATTRS = new ArrayList<String>();

    public Attributes(String[] ATTRS) {
        attributes = new ArrayList<>();
        for(int i=0; i<ATTRS.length;i++) {
            this.ATTRS.add(ATTRS[i]);
            setAttribute(i, 0);                 // Inicializa o atributo com 0 como default value
        }
    }

    @Override
    public void setAttribute(int index, Object value) {
        if(index < attributes.size()) {
            attributes.remove(index);
        }
        attributes.add(index, new Pair<>(ATTRS.get(index), value));
    }

    @Override
    public Object getAttribute(int index) {
        Pair<String, Object> pair = attributes.get(index);
        return pair;
    }

    @Override
    public void removeAttribute(int index) {
        attributes.remove(index);
    }

    public void AppendAttribute(String attribute){
        ATTRS.add(attribute);
    }

    public int gAT(String id) {
        for(int i=0; i< ATTRS.size() ; i++){
            if(ATTRS.get(i).equals(id)){
                return i;
            }
        }
        return -1;
    }
}
