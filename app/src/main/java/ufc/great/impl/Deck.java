package ufc.great.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import ufc.great.tardigrade.comunication.iPack;
import ufc.great.tardigrade.deck.iCard;
import ufc.great.tardigrade.deck.iDeck;
import ufc.great.tardigrade.utils.Flag;
import ufc.great.tardigrade.utils.NullCallback;
import ufc.great.tardigrade.utils.ReadFile;
import ufc.great.tardigrade.utils.iCallback;

public class Deck extends SQLiteOpenHelper implements iDeck {
    private static final int VERSION = 1;

    public static String DATABASE_FILE = "Tardigrade.db";
    private static String TABLE_NAME = "Deck";
    private static String CARD_ID = "uid";
    private static String CARD_NAME = "name";
    private static String CARD_DESCRIPTION = "description";

    // substitua o null com as constantes de atributo das cartas
    private static String[] CARD_ATTR = null;

    private SQLiteDatabase db;

    private List<iCard> stack;

    private iCallback onLoad = null;
    private iCallback onUseCard = null;

    public static Context mContext;

    private static Deck ourInstance = null;
    public static Deck getInstance(Context context) {
        if(ourInstance == null){
            if(context != null){
                ourInstance = new Deck(context);
                mContext = context;


                ourInstance.init();
            }
        }
        return ourInstance;
    }

    private Deck(Context context) {
        super(context, DATABASE_FILE, null, VERSION);
        stack = new ArrayList<>();
        onLoad = new NullCallback();
        onUseCard = new NullCallback();
    }

    /**
     *  Métodos do Banco de dados
     *
     */

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME + "(" +
                CARD_ID + " integer primary key," +
                CARD_NAME + " text," +
                CARD_DESCRIPTION + " text";

        for (int i = 0 ; i < CARD_ATTR.length ; i++){
            sql += ", " + CARD_ATTR[i] + " text";
        }

        sql += ")";

        db.execSQL(sql);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
    private boolean addCard(iCard icard){
        Card card = (Card) icard;

        if(getCard(card.getId()) != null){ return false; }

        ContentValues tuple = new ContentValues();
        db = this.getWritableDatabase();

        tuple.put(CARD_ID, card.getId());
        tuple.put(CARD_NAME, card.getName());
        tuple.put(CARD_DESCRIPTION, card.getDescription());
        for(int i=0; i < CARD_ATTR.length; i++){
            tuple.put(CARD_ATTR[i], card.getAttribute(CARD_ATTR[i]));
        }
        long result = db.insert(TABLE_NAME, "", tuple);
        db.close();

        if(result == -1){
            return false;
        }
        return true;
    }
    @Override
    public iCard getCard(String id) {
        iCard card = null;
        if(!id.isEmpty() || id != null) {
            String where = CARD_ID + "=" + id;

            ArrayList<String> fields = new ArrayList<>();
            fields.add(CARD_ID);
            fields.add(CARD_NAME);
            fields.add(CARD_DESCRIPTION);
            for(int i=0 ; i< CARD_ATTR.length ; i++){
                fields.add(CARD_ATTR[i]);
            }

            String[] f = new String[fields.size()];
            f = fields.toArray(f);

            db = this.getReadableDatabase();
            Cursor cursor;
            cursor = db.query(TABLE_NAME, f, where, null, null, null, null, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                String name = cursor.getString(cursor.getColumnIndexOrThrow(CARD_NAME));
                String description = cursor.getString(cursor.getColumnIndexOrThrow(CARD_DESCRIPTION));

                String[] attr = new String[CARD_ATTR.length];
                for (int i=0 ; i < CARD_ATTR.length ; i++ ){
                    attr[i] = cursor.getString(cursor.getColumnIndexOrThrow(CARD_ATTR[i]));
                }

                card = Card.Create(id, name, description, attr);
            }

            db.close();
        }
        return card;
    }
    @Override
    public List<iCard> getAllCards() {
        ArrayList<String> fields = new ArrayList<>();
        fields.add(CARD_ID);
        fields.add(CARD_NAME);
        fields.add(CARD_DESCRIPTION);
        for(int i=0 ; i< CARD_ATTR.length ; i++){
            fields.add(CARD_ATTR[i]);
        }

        String[] f = new String[fields.size()];
        f = fields.toArray(f);

        db = this.getReadableDatabase();
        Cursor cursor;
        cursor = db.query(TABLE_NAME, f, null, null, null, null, null, null);

        List<iCard> cards = new ArrayList<iCard>();
        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            boolean open = true;
            while(open){
                String id = cursor.getString(cursor.getColumnIndexOrThrow(CARD_ID));
                String name = cursor.getString(cursor.getColumnIndexOrThrow(CARD_NAME));
                String description = cursor.getString(cursor.getColumnIndexOrThrow(CARD_DESCRIPTION));

                String[] attr = new String[CARD_ATTR.length];
                for (int i=0 ; i < CARD_ATTR.length ; i++ ){
                    attr[i] = cursor.getString(cursor.getColumnIndexOrThrow(CARD_ATTR[i]));
                }

                iCard card = Card.Create(id, name, description, attr);
                cards.add(card);

                if(cursor.isLast())
                    open = false;
                else
                    cursor.moveToNext();
            }
        }

        db.close();

        return cards;
    }


    /**
     *  Métodos de Carregamento
     *
     */

    @Override
    public void init(){
        loadDeck();
    }
    @Override
    public void loadDeck(){
        ReadFile read = new ReadFile("cardbase.csv");
        read.setOnStartListener(new iCallback() {
            @Override
            public void callingBack(iPack pack) {

            }
        });

        read.setOnReadingListener(new iCallback() {
            @Override
            public void callingBack(iPack pack) {
                String[] row = (String[]) pack.getValue();
                String[] attr = null;

                String id = row[0];
                String name = row[1];
                String description = row[2];

                if(CARD_ATTR != null) {
                    attr = new String[CARD_ATTR.length];

                    for (int i = 3 ; i < 3 + CARD_ATTR.length ; i++){
                        attr[i-3] = row[i];
                    }
                }

                if(getCard(id) == null) {
                    iCard card = Card.Create(id, name, description, attr);
                    addCard(card);
                }
            }
        });

        read.setOnFinishListener(new iCallback() {
            @Override
            public void callingBack(iPack pack) {
                List<iCard> cards = getAllCards();
                iPack p = Pack.create(Flag.NOTIFY, cards);
                onLoad.callingBack(p);
            }
        });

        read.setOnFailListener(new iCallback() {
            @Override
            public void callingBack(iPack pack) {
                onUseCard.callingBack(pack);
            }
        });

        read.readAsCSV();
    }


    /**
     *  Métodos de deck
     *
     */

    @Override
    public void shuffleDeck() {
        long seed = System.nanoTime();
        Collections.shuffle(stack, new Random(seed));
    }
    @Override
    public void putCard(iCard card) {
        stack.add(getCard(card.getId()));
    }
    @Override
    public void useCard(iCard card){
        onUseCard.callingBack(Pack.create(Flag.RECOGNIZED_IN_GAME, card));
    }

    /**
     *  Métodos de eventos
     *
     */

    @Override
    public void setOnUseCardListener(iCallback callback){   onUseCard = callback;   }
    @Override
    public void setOnLoadListener(iCallback callback) {     onLoad = callback;      }
}
