package ufc.great.impl;

import ufc.great.tardigrade.game.iState;
import ufc.great.tardigrade.utils.NullCallback;
import ufc.great.tardigrade.utils.iCallback;

public class State implements iState {
    private iCallback onChangeListener;
    private iCallback onUpdateListener;
    private iCallback onMakeListener;

    private static State ourInstance = new State();

    public static State getInstance() {
        return ourInstance;
    }

    private State(){
        onChangeListener = new NullCallback();
        onMakeListener = new NullCallback();
        onUpdateListener =  new NullCallback();
    }

    // Quando este estado muda
    @Override
    public void setOnChangeListener(iCallback callback) {
        onChangeListener = callback;
    }

    // Quando este estado recebe um update
    @Override
    public void setOnUpdateListener(iCallback callback) {
        onUpdateListener = callback;
    }

    // Quando executa o processamento
    @Override
    public void setOnMakeListener(iCallback callback){
        onMakeListener = callback;
    }
}