package ufc.great.impl;


import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Intent;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.api.GoogleApiClient;

import ufc.great.components.aware.Climate;
import ufc.great.components.aware.Dice;
import ufc.great.components.aware.Light;
import ufc.great.components.aware.Shake;
import ufc.great.components.dialog.TimerDialog;
import ufc.great.tardigrade.comunication.iHub;
import ufc.great.tardigrade.utils.Flag;
import ufc.great.tardigrade.utils.NullCallback;
import ufc.great.tardigrade.utils.StatusListener;
import ufc.great.tardigrade.utils.iCallback;
import ufc.great.tardigradeteste.R;

public class Hub implements iHub {
    private static Activity mContext;
    private static Hub ourInstance = null;

    private GoogleApiClient mGoogleApiClient;

    private iCallback onChange;
    private iCallback onAware;

    private TimerDialog mCustomDialog;
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";

    private Hub() {
        onChange = new NullCallback();
        onAware = new NullCallback();
    }

    public static Hub getInstance(Activity context) {
        if (ourInstance == null) {
            if (context != null) {
                mContext = context;
                ourInstance = new Hub();
            }
        }
        return ourInstance;
    }
    public void readQRCard(){
        try {
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            mContext.startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {}
    }

    @Override
    public void start() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(Awareness.API)
                .build();
        mGoogleApiClient.connect();
        onChange.callingBack(Pack.create(Flag.NOTIFY, true));
    }

    @Override
    public void stop() {
        mGoogleApiClient.disconnect();
        onChange.callingBack(Pack.create(Flag.NOTIFY, false));
    }

    @Override
    public boolean isConnected() {
        return mGoogleApiClient.isConnected();
    }

    @Override
    public void setOnChangeListener(iCallback callback) {
        onChange = callback;
    }

    @Override
    public void setOnAwareListener(iCallback callback) {
        onAware = callback;
    }

    public void Shake(final StatusListener callback){
        mCustomDialog = TimerDialog.create(mContext);
        mCustomDialog.setProps(R.string.text_shake, R.string.tip_shake, R.drawable.shake, 3000);
        final Shake aware = new Shake(mContext, new StatusListener() {
            int cont = 0;
            @Override
            public void onStart(Object data) {      callback.onStart(data); }
            @Override
            public void onUpdate(Object data) {     cont++; }
            @Override
            public void onEnd(Object result) {
                String pre = mContext.getResources().getString(R.string.pre_text_shake);
                String pos = mContext.getResources().getString(R.string.pos_text_shake);
                mCustomDialog.changeValue(pre, cont, "", pos);
                callback.onUpdate(cont);
            }
        });
        mCustomDialog.setStatusListener(new StatusListener() {
            @Override
            public void onStart(Object data) {      aware.start();  }
            @Override
            public void onUpdate(Object data) {     aware.stop();   }
            @Override
            public void onEnd(Object result) {      callback.onEnd(result); }
        });
        mCustomDialog.show();
    }

    public void Light(final StatusListener callback){
        mCustomDialog = TimerDialog.create(mContext);
        mCustomDialog.setProps(R.string.text_light, R.string.tip_light, R.drawable.light, 3000);
        final Light aware = new Light(mContext, new StatusListener() {
            int [] light = { 0, 0, 0};
            int value = 1;

            @Override
            public void onStart(Object data) {      callback.onStart(data); }
            @Override
            public void onUpdate(Object data) {
                light[Light.getLabelIndex((float) data)] ++;
            }
            @Override
            public void onEnd(Object result) {
                if(light[0] > light[1]){
                    if(light[0] > light[2]) { value = 0; }
                    else{   value = 2;  }
                }
                else{
                    if(light[1] > light[2]) { value = 1; }
                    else{   value = 2; }
                }

                String labels = "";
                for(int i = 0 ; i < Light.labels.length; i ++){
                    if(i > 0){  labels += ",";  }
                    labels += mContext.getResources().getString(Light.labels[i]);
                }
                for(int i=0;i<3;i++){   labels += "," + labels; }
                String pre = mContext.getResources().getString(R.string.pre_text_light);
                mCustomDialog.changeValue(labels.split(","), pre, Light.getLabelIndex(value), "");
                callback.onUpdate(value);
            }
        });
        mCustomDialog.setStatusListener(new StatusListener() {
            @Override
            public void onStart(Object data) {      aware.start();  }
            @Override
            public void onUpdate(Object data) {     aware.stop(); }
            @Override
            public void onEnd(Object result) {      callback.onEnd(result); }
        });
        mCustomDialog.show();
    }

    public void ClimateCondition(final StatusListener callback){
        mCustomDialog = TimerDialog.create(mContext);
        mCustomDialog.setProps(R.string.text_weather, R.string.tip_weather, R.drawable.weather, -1);
        final Climate aware = new Climate(mContext, new StatusListener() {
            Weather value;
            @Override   public void onStart(Object data) {      callback.onStart(data); }
            @Override   public void onUpdate(Object data) {     value = (Weather) data; }
            @Override   public void onEnd(Object result) {
                String pre = mContext.getResources().getString(R.string.pre_text);
                String pos = mContext.getResources().getString(R.string.pos_text);
                if(value != null) {
                    pre = mContext.getResources().getString(R.string.pre_text_weather_condition);
                    String [] condition = new String[Climate.Conditions.length];
                    for(int i = 0 ; i < condition.length; i ++){
                        condition[i] = mContext.getResources().getString(Climate.Conditions[i]);
                    }
                    mCustomDialog.changeValue(condition, pre, value.getConditions()[0], "");
                    callback.onUpdate(result);
                }else{
                    mCustomDialog.changeValue(pre, ":(", pos, "");
                    callback.onUpdate(null);
                }
            }
        });
        mCustomDialog.setStatusListener(new StatusListener() {
            @Override   public void onStart(Object data) {      aware.start();  }
            @Override   public void onUpdate(Object data) {     /*aware.stop();*/  }
            @Override   public void onEnd(Object result) {      callback.onEnd(result); }
        });
        mCustomDialog.show();
    }

    public void ClimateTemperature(final StatusListener callback){
        mCustomDialog = TimerDialog.create(mContext);
        mCustomDialog.setProps(R.string.text_weather, R.string.tip_weather, R.drawable.weather, -1);
        final Climate aware = new Climate(mContext, new StatusListener() {
            Weather value;
            @Override
            public void onStart(Object data) {      callback.onStart(data); }
            @Override
            public void onUpdate(Object data) {     value = (Weather) data; }
            @Override
            public void onEnd(Object result) {
                String pre = mContext.getResources().getString(R.string.pre_text);
                String pos = mContext.getResources().getString(R.string.pos_text);
                if(value != null) {
                    pre = mContext.getResources().getString(R.string.pre_text_weather_temperature);
                    mCustomDialog.changeValue("", (int) value.getTemperature(Weather.CELSIUS), "ºC", "");
                    callback.onUpdate(result);
                }else{
                    mCustomDialog.changeValue(pre, ":(", pos, "");
                    callback.onUpdate(null);
                }
            }
        });
        mCustomDialog.setStatusListener(new StatusListener() {
            @Override
            public void onStart(Object data) {      aware.start();  }
            @Override
            public void onUpdate(Object data) {     /*aware.stop();*/  }
            @Override
            public void onEnd(Object result) {      callback.onEnd(result); }
        });
        mCustomDialog.show();
    }

    public void ClimateHumidity(final StatusListener callback){
        mCustomDialog = TimerDialog.create(mContext);
        mCustomDialog.setProps(R.string.text_weather, R.string.tip_weather, R.drawable.weather, -1);
        final Climate aware = new Climate(mContext, new StatusListener() {
            Weather value;
            @Override
            public void onStart(Object data) {      callback.onStart(data); }
            @Override
            public void onUpdate(Object data) {     value = (Weather) data; }
            @Override
            public void onEnd(Object result) {
                String pre = mContext.getResources().getString(R.string.pre_text);
                String pos = mContext.getResources().getString(R.string.pos_text);
                if(value != null) {
                    pre = mContext.getResources().getString(R.string.pre_text_weather_humidity);
                    mCustomDialog.changeValue(pre, value.getHumidity() , "%", "");
                    callback.onUpdate(result);
                }else{
                    mCustomDialog.changeValue(pre, ":(", pos, "");
                    callback.onUpdate(null);
                }
            }
        });
        mCustomDialog.setStatusListener(new StatusListener() {
            @Override
            public void onStart(Object data) {      aware.start();  }
            @Override
            public void onUpdate(Object data) {     aware.stop();   }
            @Override
            public void onEnd(Object result) {      callback.onEnd(result); }
        });
        mCustomDialog.show();
    }

    public void Dice(final int min, final int max, final int times, final StatusListener callback){
        mCustomDialog = TimerDialog.create(mContext);
        mCustomDialog.setProps(R.string.text_dice, R.string.tip_dice, R.drawable.random, -1);
        final Dice aware = new Dice(new StatusListener() {
            int [] values = new int[times];
            int cont = 0;
            @Override
            public void onStart(Object data) {      callback.onStart(data); }
            @Override
            public void onUpdate(Object data) {
                values[cont] = (int) data;
                cont++;
            }
            @Override
            public void onEnd(Object result) {
                String pre = mContext.getResources().getString(R.string.pre_text_dice);
                if(times > 1){
                    pre = mContext.getResources().getString(R.string.pre_text_dices);
                }

                String value = values[0] + "";
                for (int i=1; i<times; i++){
                    if(i == times - 1) {
                        value += " e " + values[i];
                    }else{
                        value += ", " + values[i];
                    }
                }
                callback.onUpdate(values);
                mCustomDialog.changeValue(pre, value , "", values);
            }
        });
        mCustomDialog.setStatusListener(new StatusListener() {
            @Override
            public void onStart(Object data) {      aware.start(min, max, times); }
            @Override
            public void onUpdate(Object data) { }
            @Override
            public void onEnd(Object result) {      callback.onEnd(result); }
        });
        mCustomDialog.show();
    }

    public static void destroy() {
        ourInstance = null;
        mContext = null;
    }
}