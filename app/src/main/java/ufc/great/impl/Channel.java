package ufc.great.impl;


import android.os.AsyncTask;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import ufc.great.tardigrade.comunication.iChannel;
import ufc.great.tardigrade.comunication.iPack;

public class Channel implements iChannel {
	private String name;
	
    private InetAddress host;
    private int port;
    private Socket channel;

    public Channel(String name, InetAddress host, int port) {
    	this.name = name;
    	this.host = host;
    	this.port = port;
    }

	@Override
	public void send(iPack pack) {
		new Send().execute(pack);
	}

	private class Send extends AsyncTask<iPack, Void, Void> {
        @Override
        protected Void doInBackground(iPack... params) {
			try {
				channel = new Socket(host, port);
				ObjectOutputStream outToServer = new ObjectOutputStream(channel.getOutputStream());
				outToServer.writeObject(params[0]);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
        }
	}

	@Override
	public String getName() {
		return this.name;
	}
	@Override
	public String getAddress() {
		if(host != null) {
			return this.host.toString();
		}
		return null;
	}
	@Override
	public int getPort() {
		return port;
	}
}
