package ufc.great.impl;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;

import java.util.UUID;

import ufc.great.tardigrade.comunication.iDiscovery;
import ufc.great.tardigrade.comunication.iNetwork;
import ufc.great.tardigrade.utils.Flag;
import ufc.great.tardigrade.utils.NullCallback;
import ufc.great.tardigrade.utils.iCallback;

public class Discovery implements iDiscovery {
    private iNetwork network;

    private String SERVICE_TYPE = "_TardigradeTeste._tcp.";
    private String SERVICE_UID = UUID.randomUUID().toString();
    private String SERVICE_NAME = Build.MODEL;

    private NsdManager mNsdManager;
    private NsdServiceInfo mNsdServiceInfo;

    private boolean init = false;
    private boolean isRegisterWorking = false;
    private boolean isDiscoveryWorking = false;

    private iCallback onStart = null;
    private iCallback onFound = null;
    private iCallback onLost = null;
    private iCallback onStop = null;
    private iCallback onFailStart = null;

    private static Discovery ourInstance = new Discovery();
    public static Discovery getInstance() {
        return ourInstance;
    }

    private Discovery() {
        mNsdServiceInfo = new NsdServiceInfo();
        mNsdServiceInfo.setServiceName(getServiceRef());
        mNsdServiceInfo.setServiceType(SERVICE_TYPE);
        mNsdServiceInfo.setPort(0);
    }

    public void setServiceId(String id){
        SERVICE_UID = id;
        mNsdServiceInfo.setServiceName(SERVICE_UID + ":" + SERVICE_NAME);
    }
    public void setServiceName(String name){
        SERVICE_NAME = name;
        mNsdServiceInfo.setServiceName(SERVICE_UID + ":" + SERVICE_NAME);
    }
    public String getServiceRef(){
        return SERVICE_UID + ":" + SERVICE_NAME;
    }
    public String getServiceName(){
        return SERVICE_NAME;
    }
    public String getServiceId(){
        return SERVICE_UID;
    }

    public void init(Context context, iNetwork network) {
        mNsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
        init(network);
    }

    @Override
    public void init(iNetwork network) {
        this.network = network;

        if(!init) {
            onStart = new NullCallback();
            onFound = new NullCallback();
            onLost = new NullCallback();
            onStop = new NullCallback();
        }

        init = true;
    }

    @Override
    public boolean start() {
        if(!init){
            return false;
        }

        if(!network.isWorking()) {
            network.start();
        }

        if(!isWorking()){
            mNsdServiceInfo.setServiceName(getServiceRef());
            mNsdServiceInfo.setServiceType(SERVICE_TYPE);
            mNsdServiceInfo.setPort(network.getPort());

            mNsdManager.registerService(mNsdServiceInfo, NsdManager.PROTOCOL_DNS_SD, mRegister);
            mNsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, mDiscovery);

            onStart.callingBack(Pack.create(Flag.RESULT, true));
            return true;
        }

        return false;
    }
    @Override
    public void stop() {
        if(!init && !isWorking()){
            return;
        }

        try{
            mNsdManager.stopServiceDiscovery(mDiscovery);
            mNsdManager.unregisterService(mRegister);
            onStop.callingBack(Pack.create(Flag.RESULT, true));
        }catch (Exception e){ }

        isDiscoveryWorking = false;
        isRegisterWorking = false;
    }

    @Override
    public void setOnStartListener(iCallback callback){
        onStart = callback;
    }
    @Override
    public void setOnFoundListener(iCallback callback){

        onFound = callback;
    }
    @Override
    public void setOnLostListener(iCallback callback){
        onLost = callback;
    }
    @Override
    public void setOnStopListener(iCallback callback){
        onStop = callback;
    }

    public void setOnFailListener(iCallback callback){ onFailStart = callback; }

    @Override
    public boolean isWorking() {
        if(!init) {
            return false;
        }

        return isDiscoveryWorking && isRegisterWorking;
    }

    private NsdManager.RegistrationListener mRegister = new NsdManager.RegistrationListener(){
        @Override
        public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            isRegisterWorking = false;
        }
        @Override
        public void onServiceRegistered(NsdServiceInfo serviceInfo) {
            isRegisterWorking = true;
        }
        @Override
        public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
            isRegisterWorking = false;
        }
        @Override
        public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            isRegisterWorking = true;
        }
    };
    private NsdManager.DiscoveryListener mDiscovery = new NsdManager.DiscoveryListener() {
        @Override
        public void onServiceFound(NsdServiceInfo serviceInfo) {
            mNsdManager.resolveService(serviceInfo, new NsdManager.ResolveListener() {
                @Override
                public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {

                }
                @Override
                public void onServiceResolved(NsdServiceInfo serviceInfo) {
                    if(!serviceInfo.getServiceName().equals(getServiceRef())) {
                        Channel mChannel = new Channel(
                                serviceInfo.getServiceName(),
                                serviceInfo.getHost(),
                                serviceInfo.getPort());
                        onFound.callingBack(Pack.create(Flag.FOUNDED, mChannel));
                    }
                }
            });
        }
        @Override
        public void onServiceLost(NsdServiceInfo serviceInfo) {
            Channel mChannel = new Channel(
                    serviceInfo.getServiceName(),
                    serviceInfo.getHost(),
                    serviceInfo.getPort());
            onLost.callingBack(Pack.create(Flag.FOUNDED, mChannel));
        }

        @Override
        public void onDiscoveryStarted(String serviceType) {
            isDiscoveryWorking = true;
        }
        @Override
        public void onDiscoveryStopped(String serviceType) {
            isDiscoveryWorking = false;
        }
        @Override
        public void onStartDiscoveryFailed(String serviceType, int errorCode) {
            onFailStart.callingBack(Pack.create(Flag.NOTIFY, errorCode));
            isDiscoveryWorking = false;
        }
        @Override
        public void onStopDiscoveryFailed(String serviceType, int errorCode) {
            isDiscoveryWorking = true;
        }
    };

    public String getIP() {
        return mNsdServiceInfo.getHost() + ":" + mNsdServiceInfo.getPort();
    }
}
