package ufc.great.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ufc.great.components.Code;
import ufc.great.tardigrade.game.iState;

public class Tuple implements iState.iTuple, Serializable {
    private Code operation;
    private List<Object> data;

    private Tuple(Code operation){
        this.operation = operation;
        data = new ArrayList<>();
    }

    public static iState.iTuple create(Code operation) {
        return new Tuple(operation);
    }

    @Override
    public Code getOperation(){
        return operation;
    }

    @Override
    public void append(Object object) {
        data.add(object);
    }

    @Override
    public List<Object> retrieve() {
        return data;
    }
}