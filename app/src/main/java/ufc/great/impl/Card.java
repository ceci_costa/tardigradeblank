package ufc.great.impl;

import android.util.Pair;

import ufc.great.tardigrade.deck.iCard;

public class Card implements iCard {
    protected String id;
    protected String name;
    protected String description;
    protected Attributes attributes;

    // Constantes de atributos

    protected Card(String id, String name, String description){
        this.id = id;
        this.name = name;
        this.description = description;

        // Declaração dos atributos aqui
    }

    @Override
    public String getId() { return id; }
    @Override
    public void   setId(String id) { this.id = id; }
    @Override
    public String getName() { return name; }
    @Override
    public void   setName(String name) { this.name = name; }
    @Override
    public String getDescription() { return description; }

    @Override
    public void setAttribute(String label, String value) {
        attributes.setAttribute(attributes.gAT(label), value);
    }

    @Override
    public String getAttribute(String label) {
        Pair<String, String> pair = (Pair<String, String>) attributes.getAttribute(attributes.gAT(label));
        return pair.second.toString();
    }

    @Override
    public void   setDescription(String description) { this.description = description; }

    @Override
    public void execute() {
        // Ação da carta
        Deck.getInstance(null).useCard(this);
    }

    @Override
    public void revert() {
        return;
    }

    public static iCard Create(String id, String name, String description, String[] attr) {
        iCard card = new Card(id, name, description);

        // Iniciar atributos baseado no parametro recebido

        return card;
    }
}
