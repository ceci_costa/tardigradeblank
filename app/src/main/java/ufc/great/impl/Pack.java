package ufc.great.impl;

import java.io.Serializable;

import ufc.great.tardigrade.comunication.iPack;

public class Pack implements Serializable, iPack {
    private static final long serialVersionUID = 1L;
	
    private String key;
    private Object value;

    private Pack(String key, Object value){
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() { return key; }
    @Override
    public void setKey(String key) { this.key = key; }

    @Override
    public Object getValue() { return value; }
    @Override
    public void setValue(Object value) { this.value = value; }

    public static iPack create(String key, Object value) {
        return new Pack(key, value);
    }
}